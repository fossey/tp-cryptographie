# -*- coding: utf-8 -*-
# Python2.7
# Romain Fossey - 2A App

from Crypto.Util import number

import random


def extended_euclid(a, b):
    d = number.GCD(a, b)

    (a0, b0) = a, b
    (u0, u1) = (1, 0)
    (v0, v1) = (0, 1)

    while b0 != 0:
        q = a0 // b0
        r = a0 - q * b0
        (a0, b0) = (b0, r)
        (u0, u1) = (u1, u0 - q * u1)
        (v0, v1) = (v1, v0 - q * v1)

    if a0 < 0:
        return d, u0 * (-1), v0 * (-1)
    else:
        return d, u0, v0


def modular_inverse(a, n):
    u = extended_euclid(a, n)

    if u[0] != 1:
        return 0

    inv = u[1]

    while inv < 0:
        inv = (inv + n) % n

    return inv


def generate_rsa_key(n):
    p = number.getStrongPrime(n)
    q = number.getStrongPrime(n)

    N = p * q

    phi = (p - 1) * (q - 1)

    while True:
        e = number.getStrongPrime(n)
        if number.GCD(e, phi) == 1:
            break

    d = modular_inverse(e, phi)

    return p, q, N, phi, d, e


def generate_rsa_crt_key(n):
    key = generate_rsa_key(n)

    p = key[0]
    q = key[1]
    d = key[4]

    dp = d % (p - 1)
    dq = d % (q - 1)
    ip = modular_inverse(p, q)
    iq = modular_inverse(q, p)

    return key + (dp, dq, ip, iq)


def sign_rsa_crt(m, key):
    N = key[2]

    if m >= N:
        return False

    p = key[0]
    q = key[1]

    dp = key[6]
    dq = key[7]

    ip = key[8]
    iq = key[9]

    sp = pow(m, dp, p)
    sq = pow(m, dq, q)

    s = iq * q * sp + ip * p * sq
    s = s % N

    return s


def sign_rsa_crt_fault(m, key):
    N = key[2]

    if m >= N:
        return False

    p = key[0]
    q = key[1]

    dp = key[6]
    dq = key[7]

    ip = key[8]
    iq = key[9]

    sp = pow(m, dp, p)
    sq = pow(m, dq, q) + random.randint(1, 100) % q

    s = iq * q * sp + ip * p * sq
    s = s % N

    return s


def sign_rsa(m, key):
    return pow(m, key[4], key[2])


def rsa_crt_bellcore(m, key):
    correct_sign = sign_rsa_crt(m, key)
    invalid_sign = sign_rsa_crt_fault(m, key)

    p = number.GCD(correct_sign - invalid_sign, key[2])
    q = key[2] / p

    return p, q


ex1 = True

if ex1:
    print "### Exercise 1\n"

    RSA_CRT_key = generate_rsa_crt_key(512)

    msg = int('baba', 16)

    print "\tMessage : ", msg

    s = sign_rsa(msg, RSA_CRT_key)

    s_CRT = sign_rsa_crt(msg, RSA_CRT_key)

    if s == s_CRT:
        print "\tSignature : ", s_CRT
    else:
        print "\tInvalid signature !"

    print "\n\tBellcore attack :\n"

    key_retrieved = rsa_crt_bellcore(msg, RSA_CRT_key)

    if key_retrieved[0] == RSA_CRT_key[0] and key_retrieved[1] == RSA_CRT_key[1]:
        print "\tKey retrieved !\n"
        print key_retrieved
    else:
        print "Error !"

    print "\n"

else:
    print "### Exercise 1 - Skipped\n"
