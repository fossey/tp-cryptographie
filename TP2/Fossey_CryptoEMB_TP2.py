# -*- coding: utf-8 -*-
# Python2.7
# Romain Fossey - 2A App

import time
import random


def update_lfsr(state, poly, n):
    output = state & 1
    feedback_bit = 0

    for i in range(n):
        feedback_bit ^= (((state >> i) & 1) & ((poly >> (n - i)) & 1))

    state = (state >> 1) ^ (feedback_bit << (n - 1))
    return output, state


def key_stream_lfsr(state, poly, n, l):
    key_stream = 0
    for i in range(l):
        output, state = update_lfsr(state, poly, n)
        key_stream ^= (output << i)
    return key_stream


def Bool(x1, x2, x3):
    x = x3 ^ (x2 << 1) ^ (x1 << 2)
    return (226 >> x) & 1  # 226 = 11100010 en binaire


# P1(X) = X^10 + X^3 + 1
def poly_1():
    return (1 << 10) ^ (1 << 3) ^ 1


# P2 = X^11 + X^2 + X^10 + 1
def poly_2():
    return (1 << 11) ^ (1 << 2) ^ 1


# P3 = X^12 + X^6 + X^4 + X + 1
def poly_3():
    return (1 << 12) ^ (1 << 6) ^ (1 << 4) ^ (1 << 1) ^ 1


def Geffe(key, l):
    # Supprime 0b
    key = bin(key)[2:]

    # Ajoute les 0 non-significatif
    while len(key) < 33:
        key = "0" + key

    state_1 = int(key[23:33], 2)
    state_2 = int(key[12:23], 2)
    state_3 = int(key[0:12], 2)

    # print bin(int(key,2))
    # print bin(concat_LFSR(state_1,state_2,state_3))

    # print state_1
    # print state_2
    # print state_3

    keystream_1 = key_stream_lfsr(state_1, poly_1(), 10, l)
    keystream_2 = key_stream_lfsr(state_2, poly_2(), 11, l)
    keystream_3 = key_stream_lfsr(state_3, poly_3(), 12, l)

    # print keystream_1
    # print keystream_2
    # print keystream_3

    geffe_key_stream = 0

    for i in range(l):
        geffe_key_stream ^= Bool((keystream_1 >> i) & 1, (keystream_2 >> i) & 1, (keystream_3 >> i) & 1) << i

    return geffe_key_stream


def statistical_test(s1, s2, l):
    proportion = 0

    for i in range(l):
        if ((s1 >> i) & 1) != ((s2 >> i) & 1):
            proportion += 1

    if proportion < (l / 3.0):
        return True
    else:
        return False


def reverse_Geffe(cypher, l):
    lfr1_iv = 0

    # Incrémente l'état initial potential dun LFSR 1 jusqu'à ce que
    # celui-ci soit fortement corrélé avec la sortie du registre
    while True:
        lfr1_keystream = key_stream_lfsr(lfr1_iv, poly_1(), 10, l)
        if statistical_test(lfr1_keystream, cypher, l):
            break;
        else:
            lfr1_iv += 1

    lfr3_iv = 0

    # Idem avec le LFSR 3
    while True:
        lfr3_keystream = key_stream_lfsr(lfr3_iv, poly_3(), 12, l)
        if statistical_test(lfr3_keystream, cypher, l):
            break;
        else:
            lfr3_iv += 1

    # print lfr1_iv
    # print lfr3_iv

    lfr2_iv = 0

    # On admets que l'on a trouvé les états initiaux des LFSR 1 et 3
    # puis on teste toute les possibilités du LFSR 2 jusqu'à ce qu'on obtienne le même chiffré
    while True:
        potential_key = concat_lfsr(lfr1_iv, lfr2_iv, lfr3_iv)
        if cypher == Geffe(potential_key, l):
            break;
        else:
            lfr2_iv += 1

    return potential_key


def concat_lfsr(lfr1_iv, lfr2_iv, lfr3_iv):
    iv = lfr3_iv
    iv = (iv << 11) | lfr2_iv
    iv = (iv << 10) | lfr1_iv

    return iv


ex1 = True
ex2 = True

if ex1:
    iv = 2015686042
    cycles = 30
    print "### Exercise 1\n"
    print "\tIV : ", iv
    print "\tCycles : ", cycles

    t0 = time.clock()
    geffe_code = Geffe(iv, cycles)
    t1 = time.clock() - t0

    print "\tResults : ", geffe_code
    print "\tBinary Results : ", bin(geffe_code)
    print "\tExecution time : ", t1, "sec\n"

else:
    print "### Exercise 1 - Skipped\n"

if ex2:
    print "### Exercise 2\n"

    l = 300
    key = random.getrandbits(33)
    cypher = Geffe(key, l)

    print "\tKey : ", key
    print "\tCycle : ", l
    print "\tCypher : ", cypher

    t0 = time.clock()
    key_retrieved = reverse_Geffe(cypher, l)
    t1 = time.clock() - t0

    print "\tKey retrieved :", key_retrieved
    print "\tExecution time : ", t1, "sec\n"

else:
    print "### Exercise 2 - Skipped\n"
