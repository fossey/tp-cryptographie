# -*- coding: utf-8 -*-
# Python2.7
# Romain Fossey - 2A App

import time
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from Crypto.Util import number


def get_min_concat_end_with_lol(string):
    obj = SHA256.new()
    count = 0

    while obj.hexdigest()[58:64] != "4c4f4c":
        obj.update(string)
        count += 1

    return count


def get_128_bits_random_key():
    with open("/dev/random", 'r') as f:
        raw_key = f.read(16)
    return raw_key


def get_cypher_message_ends_with_lol(message):
    print "\tMessage : ", msg

    key = get_128_bits_random_key()

    while True:
        x = AES.new(key, AES.MODE_ECB)
        c = x.encrypt(message)

        if c[-3:] == "LOL":
            break
        else:
            key = number.bytes_to_long(key)
            key += 1
            key = number.long_to_bytes(key)

    print "\tCypher message : ", c
    print "\tDecipher message : ", x.decrypt(c)


ex1 = True
ex2 = True

if ex1:
    print "### Exercise 1"

    t0 = time.clock()
    cpt = get_min_concat_end_with_lol("Romain")
    t1 = time.clock() - t0

    print "\tMultiplicator  :", cpt
    print "\tExecution time : ", t1, "sec\n"

    print "\t----- Verification -----"
    verification = SHA256.new()
    verification.update(cpt * 'Romain')
    print "\tHASH - hex ", verification.hexdigest(), " - bin ", verification.digest()
else:
    print "### Exercise 1 - Skipped\n"

if ex2:
    print "### Exercise 2"

    msg = "_I_LOVE_CRYPTO__"

    t0 = time.clock()
    get_cypher_message_ends_with_lol(msg)  # environ 4min30 avec ce message
    t1 = time.clock() - t0

    print "\tExecution time : ", t1, "sec"

else:
    print "### Exercise 2 - Skipped"
