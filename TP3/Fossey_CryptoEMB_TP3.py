# -*- coding: utf-8 -*-
# Python2.7
# Romain Fossey - 2A App

import math
import random


def euclid(a, b):
    if b == 0 or a == 0:
        return max(a, b)

    r = 1

    while a % b != 0:
        r = a % b
        a = b
        b = r

    return abs(r)


def extended_euclid(a, b):
    d = euclid(a, b)
    r = 0

    (a0, b0) = a, b
    (u0, u1) = (1, 0)
    (v0, v1) = (0, 1)

    while b0 != 0:
        q = a0 // b0
        r = a0 - q * b0
        (a0, b0) = (b0, r)
        (u0, u1) = (u1, u0 - q * u1)
        (v0, v1) = (v1, v0 - q * v1)

    if a0 < 0:
        return d, u0 * (-1), v0 * (-1)
    else:
        return d, u0, v0


def modular_inverse(a, n):
    u = extended_euclid(a, n)

    if u[0] != 1:
        return 0

    inv = u[1]

    while inv < 0:
        inv = (inv + n) % n

    return inv


def checkpoint(a, b, p, P):
    if P[2] == 0:
        return True

    if pow(P[1], 2) % p == (pow(P[0], 3) + a * P[0] + b) % p:
        return True
    else:
        return False


def addition_points(A, B, p, P, Q):
    if P[2] == 0:
        return Q

    if Q[2] == 0:
        return P

    if P[0] == Q[0] and P[1] != Q[1]:
        return 0, 0, 0

    if P[0] != Q[0]:
        L = (Q[1] - P[1]) * modular_inverse(Q[0] - P[0], p) % p
        X = (pow(L, 2) - Q[0] - P[0]) % p
        Y = (L * (P[0] - X) - P[1]) % p

        return X, Y, 1

    if P[0] == Q[0] and P[1] == Q[1] and P[1] == 0:
        return 0, 0, 0

    if P[0] == Q[0] and P[1] == Q[1] and P[1] != 0:
        L = ((3 * pow(P[0], 2) + A) * modular_inverse(2 * Q[1], p)) % p
        X = (pow(L, 2) - 2 * P[0]) % p
        Y = (L * (P[0] - X) - P[1]) % p

        return X, Y, 1


def double_and_add(A, B, p, P, k):
    Q = (0, 0, 0)

    if k == 0:
        return Q

    for i in range(int(math.log(k, 2)) + 1, -1, -1):
        Q = addition_points(A, B, p, Q, Q)
        if (k >> i) & 1:
            Q = addition_points(A, B, p, Q, P)

    return Q


def groupe_des_points(A, B, p):
    points = []
    points.append((0, 0, 0))

    for i in range(p):
        for j in range(p):

            P = (i, j, 1)

            if checkpoint(A, B, p, P) and not P in points:
                points.append(P)

    return points


def generateurs(A, B, p):
    points = groupe_des_points(A, B, p)
    generators = []

    for P in points[1:]:
        X = P
        k = 1

        while True:
            X = addition_points(A, B, p, X, P)

            if X[2] == 0:
                break;
            else:
                k += 1

        if k == len(points) - 1:
            generators.append(P)

    return generators


def est_cyclique(A, B, p):
    if len(groupe_des_points(A, B, p)) - 1 == len(generateurs(A, B, p)):
        return True
    else:
        return False


def DiffieHelman(A, B, p, n, G):
    a = random.randint(1, n)
    b = random.randint(1, n)

    # print "a = ", a
    # print "b = ", b

    aG = double_and_add(A, B, p, G, a)
    bG = double_and_add(A, B, p, G, b)

    # print "aG = ", aG
    # print "bG = ", bG

    baG = double_and_add(A, B, p, aG, b)
    abG = double_and_add(A, B, p, bG, a)

    print "baG = ", baG
    print "abG = ", abG


def sign_ECDSA(A, B, p, P, n, i, msg):
    k = random.randint(1, n - 1)
    K = double_and_add(A, B, p, P, k)
    t = K[0]
    s = (msg + i * t) * modular_inverse(k, n) % n

    # print "k=", k
    # print "t=", t
    # print "s=", s

    return (s, t)


def verif_ECDSA(A, B, p, P, n, H, msg, signature):
    if signature[0] < 1 and signature[0] > n - 1:
        return False

    if signature[1] < 1 and signature[1] > n - 1:
        return False

    Q = addition_points(A, B, p,
                        double_and_add(A, B, p, P, msg * modular_inverse(signature[0], n)),  # ms⁻¹P
                        double_and_add(A, B, p, H, signature[1] * modular_inverse(signature[0], n))  # ts⁻¹H
                        )

    if Q[0] % n == signature[1]:
        return True
    else:
        return False


ex1 = True
ex2 = True

if ex1:

    print "### Exercise 1\n"

    A = 3
    B = 2
    p = 5

    print "\tElliptic Curve -> Y² = X³-", A, "X +", B, " on Z", p, "\n"
    print "\tAdditions"
    print "\t\t(2,1) + (2,4) =", addition_points(A, B, p, (2, 1, 1), (2, 4, 1))
    print "\t\t(2,1) + (2,1) =", addition_points(A, B, p, (2, 1, 1), (2, 1, 1))
    print "\t\t(2,1) + (0,0) =", addition_points(A, B, p, (2, 1, 1), (0, 0, 0))
    print "\t\t(2,1) + (1,1) =", addition_points(A, B, p, (2, 1, 1), (1, 1, 1))
    print "\t\t(2,1) + (1,4) =", addition_points(A, B, p, (2, 1, 1), (1, 4, 1))
    print "\t\t(2,1) + (1,4) =", addition_points(A, B, p, (1, 4, 1), (1, 4, 1))

    P = (2, 4, 1)
    print "\n\tMultiply P=", P, "by k"

    for k in range(6):
        print "\t\t", k, "* P = ", double_and_add(A, B, p, P, k)

    points = groupe_des_points(A, B, p)
    print "\n\tNumber of element for this curve :", len(points)

    for point in points:
        print "\t\t", point

    generators = generateurs(A, B, p)

    print "\n\tNumber of generator elements : ", len(generators)

    for point in generators:
        print "\t\t", point

    print"\n\tCycle curve : ", est_cyclique(A, B, p), "\n"
else:
    print "### Exercise 1 - Skipped\n"

if ex2:
    print "### Exercise 2\n"

    # A = 2
    # B = 1
    # p = 7
    # n = 5
    # P = (0,1,1)

    A = -3
    B = int('5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b', 16)

    n = 115792089210356248762697446949407573529996955224135760342422259061068512044369
    p = 115792089210356248762697446949407573530086143415290314195533631308867097853951

    Px = int('6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296', 16)
    Py = int('4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5', 16)

    P = (Px, Py, 1)

    print "\tDiffie Helman - ECDSA (with P256)"

    DiffieHelman(A, B, p, n, P)

    print "\n\tSignature and verification of ECDSA with P256 (FIPS 186-4)\n"

    msg = int('baba', 16)

    print "\tMessage : ", msg

    i_privateKey = 4
    H_publicKey = double_and_add(A, B, p, P, i_privateKey)

    signature = sign_ECDSA(A, B, p, P, n, i_privateKey, msg)

    print "\tSignature : ", signature

    verification = verif_ECDSA(A, B, p, P, n, H_publicKey, msg, signature)

    if verification:
        print "\tVerification : Authentic"
    else:
        print "\tVerification : Falsified"

else:
    print "### Exercise 2 - Skipped\n"
